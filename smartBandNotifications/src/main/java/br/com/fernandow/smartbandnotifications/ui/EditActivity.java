/*
 * Copyright 2013 two forty four a.m. LLC <http://www.twofortyfouram.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package br.com.fernandow.smartbandnotifications.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;

import br.com.fernandow.smartbandnotifications.R;
import br.com.fernandow.smartbandnotifications.bundle.BundleScrubber;
import br.com.fernandow.smartbandnotifications.bundle.PluginBundleManager;

public final class EditActivity extends AbstractPluginActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BundleScrubber.scrub(getIntent());

        final Bundle localeBundle = getIntent().getBundleExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE);
        BundleScrubber.scrub(localeBundle);

        addPreferencesFromResource(R.xml.pref_edit);
        
        final ListPreference actionPreference = (ListPreference) findPreference("action");
        final EditTextPreference countPreference = (EditTextPreference) findPreference("count");
        final EditTextPreference intervalPreference = (EditTextPreference) findPreference("interval");
               
        actionPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
            	final int val = Integer.parseInt(newValue.toString());
            	setPreferenceEnabled(countPreference, intervalPreference, val);
        		return true;
        	}
        });

        
        if (null == savedInstanceState) {
            if (PluginBundleManager.isBundleValid(localeBundle)) {
                
            	final int action = localeBundle.getInt(PluginBundleManager.BUNDLE_EXTRA_INT_ACTION);
            	final int count = localeBundle.getInt(PluginBundleManager.BUNDLE_EXTRA_INT_NOTIFICATION_COUNT);
            	final int interval = localeBundle.getInt(PluginBundleManager.BUNDLE_EXTRA_INT_INTERVAL);
            	
            	setIntegerPreference("count", count);
            	setIntegerPreference("interval", interval);
            	setPreferenceEnabled(countPreference, intervalPreference, action);

            }
        }
    }
    
    private static void setPreferenceEnabled(Preference countPreference, Preference intervalPreference, int val) {
    	if (val == 1) {
			countPreference.setEnabled(true);
			intervalPreference.setEnabled(true);
		} else { 
			countPreference.setEnabled(false);
			intervalPreference.setEnabled(false);
		}
    }

	public int getIntegerPreference(String key, int def) {
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String valor = preferences.getString(key, def + "");
	    return (!valor.isEmpty() ? Integer.parseInt(valor) : 0);
	}
	
	public void setIntegerPreference(String key, int value){
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	    SharedPreferences.Editor editor = preferences.edit();
	    editor.putString(key, value + "");
	    editor.commit();
	}

    @Override
    public void finish() {
        if (!isCanceled()) {
       	
        	// Action
        	final int action_default = 1;
        	final int action = getIntegerPreference("action", action_default);
        	
        	// Count
        	final int count_default = 0;
        	int count_num = getIntegerPreference("count", count_default);
        	String count_text = count_num + "";
        	if (count_num <= 0) {
        		count_num = 0;
        		count_text = getResources().getString(R.string.infinite);
        	}
        	final int count = count_num;
        	
        	// Interval
        	final int interval_default = 2000;
        	final int interval_min = 1000;
        	int interval_num = getIntegerPreference("interval", interval_default);
        	if (interval_num < interval_min) {
        		interval_num = interval_min;
        	}
        	final int interval = interval_num;
        	
        	// Blurb
        	String blurb = "";
        	if (action == 1) {
        		blurb = getResources().getString(R.string.info, count_text, interval);;
        	} else {
        		blurb = getResources().getString(R.string.cancel_all_notifications);
        	}
            
        	// Return
        	final Intent resultIntent = new Intent();
			final Bundle resultBundle = PluginBundleManager.generateBundle(getApplicationContext(), action, count, interval);
			resultIntent.putExtra(com.twofortyfouram.locale.Intent.EXTRA_BUNDLE, resultBundle);
			resultIntent.putExtra(com.twofortyfouram.locale.Intent.EXTRA_STRING_BLURB, blurb);
			setResult(RESULT_OK, resultIntent);
			
        }

        super.finish();
    }

}