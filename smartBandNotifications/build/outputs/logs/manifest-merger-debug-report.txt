-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:tools
		ADDED from AndroidManifest.xml:3:5
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:7:5
	android:versionCode
		ADDED from AndroidManifest.xml:6:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:installLocation
		ADDED from AndroidManifest.xml:5:5
uses-sdk
ADDED from AndroidManifest.xml:9:5
MERGED from smartband-notify:localeapi:unspecified:5:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:11:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:10:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-feature#android.hardware.touchscreen
ADDED from AndroidManifest.xml:14:5
MERGED from smartband-notify:localeapi:unspecified:9:5
	android:required
		ADDED from AndroidManifest.xml:16:9
	android:name
		ADDED from AndroidManifest.xml:15:9
supports-screens
ADDED from AndroidManifest.xml:19:5
	android:largeScreens
		ADDED from AndroidManifest.xml:21:9
	android:anyDensity
		ADDED from AndroidManifest.xml:20:9
	android:normalScreens
		ADDED from AndroidManifest.xml:22:9
	android:xlargeScreens
		ADDED from AndroidManifest.xml:24:9
	android:smallScreens
		ADDED from AndroidManifest.xml:23:9
application
ADDED from AndroidManifest.xml:26:5
	android:label
		ADDED from AndroidManifest.xml:31:9
	android:allowBackup
		ADDED from AndroidManifest.xml:28:9
	android:uiOptions
		ADDED from AndroidManifest.xml:33:9
	android:icon
		ADDED from AndroidManifest.xml:30:9
	android:theme
		ADDED from AndroidManifest.xml:32:9
	android:hardwareAccelerated
		ADDED from AndroidManifest.xml:29:9
	android:name
		ADDED from AndroidManifest.xml:27:9
activity#br.com.fernandow.smartbandnotifications.ui.InfoActivity
ADDED from AndroidManifest.xml:36:9
	android:icon
		ADDED from AndroidManifest.xml:38:13
	android:exported
		ADDED from AndroidManifest.xml:39:13
	android:name
		ADDED from AndroidManifest.xml:37:13
	tools:ignore
		ADDED from AndroidManifest.xml:40:13
intent-filter#android.intent.action.MAIN+android.intent.category.INFO+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:41:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:42:17
	android:name
		ADDED from AndroidManifest.xml:42:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:43:5
	android:name
		ADDED from AndroidManifest.xml:43:15
category#android.intent.category.INFO
ADDED from AndroidManifest.xml:44:17
	android:name
		ADDED from AndroidManifest.xml:44:27
activity#br.com.fernandow.smartbandnotifications.ui.EditActivity
ADDED from AndroidManifest.xml:48:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:55:13
	android:label
		ADDED from AndroidManifest.xml:52:13
	android:uiOptions
		ADDED from AndroidManifest.xml:54:13
	android:icon
		ADDED from AndroidManifest.xml:51:13
	android:theme
		ADDED from AndroidManifest.xml:53:13
	android:exported
		ADDED from AndroidManifest.xml:50:13
	android:name
		ADDED from AndroidManifest.xml:49:13
	tools:ignore
		ADDED from AndroidManifest.xml:56:13
intent-filter#com.twofortyfouram.locale.intent.action.EDIT_SETTING
ADDED from AndroidManifest.xml:57:13
action#com.twofortyfouram.locale.intent.action.EDIT_SETTING
ADDED from AndroidManifest.xml:58:17
	android:name
		ADDED from AndroidManifest.xml:58:25
receiver#br.com.fernandow.smartbandnotifications.receiver.FireReceiver
ADDED from AndroidManifest.xml:61:9
	android:icon
		ADDED from AndroidManifest.xml:64:13
	android:process
		ADDED from AndroidManifest.xml:65:13
	android:exported
		ADDED from AndroidManifest.xml:63:13
	android:name
		ADDED from AndroidManifest.xml:62:13
	tools:ignore
		ADDED from AndroidManifest.xml:66:13
intent-filter#com.twofortyfouram.locale.intent.action.FIRE_SETTING
ADDED from AndroidManifest.xml:67:13
action#com.twofortyfouram.locale.intent.action.FIRE_SETTING
ADDED from AndroidManifest.xml:68:17
	android:name
		ADDED from AndroidManifest.xml:68:25
