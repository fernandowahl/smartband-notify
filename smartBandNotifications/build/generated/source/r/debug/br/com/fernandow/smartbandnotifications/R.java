/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package br.com.fernandow.smartbandnotifications;

public final class R {
    public static final class array {
        public static final int action=0x7f090000;
        public static final int actionValues=0x7f090001;
    }
    public static final class attr {
    }
    public static final class drawable {
        public static final int twofortyfouram_locale_ic_menu_dontsave=0x7f020000;
        public static final int twofortyfouram_locale_ic_menu_help=0x7f020001;
        public static final int twofortyfouram_locale_ic_menu_save=0x7f020002;
    }
    public static final class id {
        public static final int TextView01=0x7f0a0005;
        public static final int TextView02=0x7f0a0006;
        public static final int radioButton1=0x7f0a0003;
        public static final int radioButton2=0x7f0a0004;
        public static final int twofortyfouram_locale_menu_dontsave=0x7f0a0000;
        public static final int twofortyfouram_locale_menu_help=0x7f0a0001;
        public static final int twofortyfouram_locale_menu_save=0x7f0a0002;
    }
    public static final class integer {
        public static final int twofortyfouram_locale_maximum_blurb_length=0x7f070000;
    }
    public static final class layout {
        public static final int activity_edit=0x7f040000;
    }
    public static final class menu {
        public static final int twofortyfouram_locale_help_save_dontsave=0x7f0b0000;
    }
    public static final class mipmap {
        public static final int ic_launcher=0x7f030000;
    }
    public static final class string {
        public static final int action=0x7f060005;
        public static final int app_name=0x7f060019;
        public static final int cancel=0x7f060006;
        public static final int cancel_all_notifications=0x7f060007;
        public static final int cancel_notifications=0x7f060008;
        public static final int define_number=0x7f060009;
        public static final int define_whether=0x7f06000a;
        public static final int infinite=0x7f06000b;
        public static final int info=0x7f06000c;
        public static final int interval_hint=0x7f06000d;
        public static final int interval_notes=0x7f06000e;
        public static final int minimum_milliseconds=0x7f06000f;
        public static final int notification=0x7f060010;
        public static final int notification_interval=0x7f060011;
        public static final int notification_number=0x7f060012;
        public static final int notification_text=0x7f060013;
        public static final int notify=0x7f060014;
        public static final int notify_hint=0x7f060015;
        public static final int one=0x7f06001a;
        public static final int plugin_name=0x7f06001b;
        public static final int provide_number=0x7f060016;
        public static final int summary_notifications=0x7f060017;
        public static final int trigger_notifications=0x7f060018;
        public static final int two=0x7f06001c;
        public static final int twofortyfouram_locale_breadcrumb_format=0x7f060000;
        public static final int twofortyfouram_locale_breadcrumb_separator=0x7f060001;
        public static final int twofortyfouram_locale_menu_dontsave=0x7f060002;
        public static final int twofortyfouram_locale_menu_help=0x7f060003;
        public static final int twofortyfouram_locale_menu_save=0x7f060004;
    }
    public static final class style {
        public static final int MotorolaListViewHackStyle=0x7f080003;
        public static final int Theme_Locale_Dark=0x7f080000;
        public static final int Theme_Locale_Dialog=0x7f080001;
        public static final int Theme_Locale_Light=0x7f080002;
    }
    public static final class xml {
        public static final int pref_edit=0x7f050000;
    }
}
